{
    "meta": {
        "title": "One Summer Night",
        "author": "Bill Jones"
    },
    "slides": [
        {
            "src": "/img/1.jpg",
            "duration": 4488
        },
        {
            "src": "/img/2.jpg",
            "duration": 5180
        },
        {
            "src": "http://www.today.it/~media/zoom/1925737111685/anna-safroncik-primo-piano.jpg",
            "duration": 5001
        },
        {
            "src": "/img/4.jpg",
            "duration": 5201,
            "poi": {
                "x": 0.5, 
                "y": 0.6 
            }
        },
        {
            "src": "/img/5.jpg",
            "duration": 4999
        }
    ],
    "audio": [
        "/audio/1.mp3",
        "/audio/1.ogg"
    ],
    "links": [
        {
            "title": "First Link",
            "src": "http://firstlink.com/first"
        },
        {
            "title": "Second Link",
            "src": "http://firstlink.com/2"
        }
    ]
}