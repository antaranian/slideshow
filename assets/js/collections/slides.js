define([
	'jquery',
	'backbone',
	'models/slide',
	'views/slides',
	'views/controls'
], function($, Backbone, Slide,  SlidesView, ControlsView) {
	var Collection = Backbone.Collection.extend({
			step: 0,
			model: Slide,
			initialize: function(data){
				this.view = new SlidesView(this);
				this.controls = new ControlsView(this);

				this.delegate(data);
			},
			delegate: function(slides){
				var self = this;

				var todo = slides.length;

				this.on('onedone', function(){
					todo--;
					if (!todo) {
						// show the first
						this.pre();
					}
				});

				app.audio.on('all', function(ev){
					var fn = self[ev];
					fn && fn.apply(self);
				}, self);
			},
			next: function(ms){
				this.step++;
				if (this.length > this.step) {
					this.play(ms, true);
				}
			},
			play: function(ms, nth){
				this.trigger('playing', true);
				var slide = this.at(this.step),
					ms = ms || slide.get('duration') / 10;
				slide.play(ms, nth);
			},
			pause: function(){
				this.trigger('playing', false);
				this.at(this.step).pause();
			},
			finish: function(){
				var self = this;
				setTimeout(function(){
					if (self.step < self.length) {
						self.finish();
						return false;
					}
					setTimeout(function(){
						self.at(0).view.show();
						self.trigger('stop');
						self.pre();
					}, appData.lastTimeout);
				}, 100);
			},
			pre: function(){
				this.trigger('loaded');
				this.step = 0;
			}
		});
	return Collection;
});