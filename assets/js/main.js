require.config({
	paths: {
		// Core Libraries
		'jquery'	: 'libs/jquery',
		'underscore': 'libs/lodash',
		'backbone'	: 'libs/backbone',
		'sound'		: 'libs/soundmanager2',
		// jQuery Plugins
		'pause'		: 'plugins/jquery.pause'
	},
	shim: {
		// Soundmanager2
		'audio': {
			'exports': 'soundManager'
		},
		// jQuery .animate() pause/resume plugin
		'pause': ['jquery'],
		// Backbone
		'backbone': {
			'deps': ['underscore', 'jquery'],
			'exports': 'Backbone' 
		}
	}
});

require([
	'app',
	'pause'
], function(App) {
	window.app = App();
	app.init(appData);
});