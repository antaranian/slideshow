define([
	'jquery',
	'underscore',
	'backbone',
	'views/contacts/contact'
], function($, _, Backbone, ContactView) {
	var Model = Backbone.Model.extend({
			defaults: {
				_id	:	null
			},
			initialize: function(){
				this.view = new ContactView(this);
			}
		});
	return Model;
});