define([
	'jquery',
	'underscore',
	'backbone',
	'sound'
], function($, _, Backbone) {
	var Model = Backbone.Model.extend({
			defaults: {	},
			initialize: function(){
				var self = this;
				soundManager.setup({
					url: 'swf/',
					flashVersion: 9, 
					preferFlash: false,
					useFlashBlock: false,
					onready: function(){
						self.setup()
					}
				});
			},
			setup: function(){
				var self = this;
				self.audio = soundManager.createSound({
					id: 'sound1',
					url: 'audio/1.mp3',
					onload: function(){
						// console.log(this)
						// self.play = this.play
					},
					onplay: function(){
						self.trigger('play');
					},
					onpause: function(){
						self.trigger('pause');
					},
					onresume: function(){
						self.trigger('play');
					},
					onstop: function(){
						self.trigger('stop');
					},
					onfinish: function() {
						self.trigger('finish');
					}

				});
			},
			play: function(){
				// console.log(4)
				this.audio.play();
			},
			pause: function(){
				this.audio.pause();
			},
			stop: function(){
				this.audio.stop();
			}
		});
	return Model;
});