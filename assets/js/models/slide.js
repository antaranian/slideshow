define([
	'jquery',
	'underscore',
	'backbone',
	'views/slide'
], function($, _, Backbone, SlideView) {
	function rand (from, to){
		to = to || 1;

		to *= 10;
		from *= 10;

		return Math.floor(Math.random() * (to - from + 1) + from) / 10;
	};

	var Model = Backbone.Model.extend({
			defaults: {

			},
			initialize: function(){
				this.view = new SlideView(this);

				this.delegate();
			},
			delegate: function(){
				this.on('change:size', function(m, size){
					this.set(this.kenburns());
					this.collection.trigger('onedone', this.cid);
					if (this.collection.indexOf(this) === 0) {
						this.view.show();
					}
					// console.log(this.toJSON());
				});

				this.on('played', function(){
					var ms = this.get('duration') / 10;
					this.collection.next(ms);
				});

				this.on('finished', function(){
					this.collection.trigger('finished');
				});
			},
			play: function(ms, nth){
				this.view.play(ms, nth);
			},
			pause: function(){
				this.view.pause();
			},
			kenburns: function(){
				var narrow, wide,
					size = this.get('size').width;

				var scopeDuration = Math.min(this.get('duration'), 5000),
					scopeLB = 1 - 3 * (0.3 * (scopeDuration) / 5000) / 2,
					scopeRB = 1 - (0.3 * (scopeDuration) / 5000) / 2,
					scope = rand(scopeLB,scopeRB);

				var poi = this.get('poi') || { x: rand(.4, .6), y: rand(.4, .6) };
				// zoom = poi ? rand(.5) : rand(.75);

				var x1, y1, x2, y2,
					x = poi.x, y = poi.y;

				if (x - scope/2 < 0) {
					x1 = 0;
					x2 = scope;
				} else if (x + scope/2 > 1) {
					x1 = 1 - scope;
					x2 = 1;
				} else {
					x1 = x - scope/2;
					x2 = x + scope/2;
				}

				if (y - scope/2 < 0) {
					y1 = 0;
					y2 = scope;
				} else if (y + scope/2 > 1) {
					y1 = 1 - scope;
					y2 = 1;
				} else {
					y1 = y - scope/2;
					y2 = y + scope/2;
				}

				var c = 1 / scope;

				narrow = {
					top: -y1 * size * c,
					left: -x1 * size * c,
					width: size * c,
					height: size * c
				};
				wide = {
					top: 0,
					left: 0,
					width: size,
					height: size
				};

				return !this.get('poi') ? 
					{ first: narrow, last: wide } :
					{ first: wide, last: narrow };

			}
		});
	return Model;
});