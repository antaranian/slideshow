define([
	'jquery',
	'underscore',
	'backbone',
	'collections/contacts'
], function($, _, Backbone, Contacts) {
	var Model = Backbone.Model.extend({
			defaults: {
				_id	:	false
			},
			initialize: function(){
				this.contacts = new Contacts([], this);
			},
			login: function(){
				var self = this;

				$.ajax('/api/user', {
					dataType: 'json',
					type: 'get',
					context: this,
					complete: function(xhr, status){
						this.trigger('login login' + status);
					},
					success: this.set
				});
			}
		});
	return Model;
});