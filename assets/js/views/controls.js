define([
	'jquery',
	'underscore',
	'backbone',
], function($, _, Backbone){
	var View = Backbone.View.extend({
			events: {
				'click #play': function(){
					app.play();
					return false;
				},
				'click #pause': function(){
					app.pause();
					return false;
				}
			},
			initialize: function(model){
				this.model = model;

				this.$el = $('#controls');

				this.delegate();

				this.render();
				// console.log(markupAlert)
			},
			render: function(){
				this.$('#title').text(app.meta.title);
				this.$('#author').text(app.meta.author);
			},
			delegate: function(){
				this.model.on('playing', function(playing){
					var fn = playing ? 'addClass' : 'removeClass';
					$('#container')[fn]('playing');
				});			
			}
		});
	return View;
});