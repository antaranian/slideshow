define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone){
	var View = Backbone.View.extend({
			initialize: function(model){
				this.model = model;

				this.$el = $('#slides');

				this.delegate();

				this.render();
			},
			render: function(){

			},
			delegate: function(){
				var self = this,
					model = self.model;

				var $container = $('#container'),
					$ul = self.$el;

				model.on('loaded', function(){
					$container
						.delay(500)
						.addClass('load')
						.find('#slides li:not(:first) img')
							.fadeTo('fast', 0);
				});

				model.on('stop', function(){
					$container
						.addClass('played')
						.removeClass('playing');
				});
			}
		});
	return View;
});