define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone, tplUserPanel){
	var View = Backbone.View.extend({
			tagName: 'li',
			initialize: function(model){
				this.model = model;

				this.render();
			},
			render: function(){
				var self = this,
					model = self.model, 
					data = model.toJSON();

				var $img = $('<img />')
						.attr('src', data.src)
						.appendTo(this.el);
				$img.on('load', function(a, b){
					model.set('size', {
						height	: $img.height(),
						width	: $img.width() 
					});
				});
				this.$img = $img;

				$('#slides').append(this.el);
			},
			show: function(){
				var data = this.model.toJSON();
				this.$img
					.css(data.first)
					.fadeTo('fast', 1);
			},
			play: function(ms, nth){
				var self = this,
					data = self.model.toJSON();

				if (this.paused) {
					self.$img.resume();
					return true;
				} 
				this.paused = false;

				var done = false;
				var dTime = data.duration / 10;

				self.$img
					.css(data.first)
					.animate({ opacity: 1 }, { duration: ms + dTime, easing: 'linear', queue: false })
					.animate(data.last, {
						duration: 10 * dTime, 
						easing: 'linear',
						step: function(a, b){
							if (!done && b.pos >= .9) {
								done = true
								self.model.trigger('played');
							} 
						},
						complete: function(){
							self.model.trigger('finished');
						}
				});
			},
			pause: function(){
				this.$img.pause();
				this.paused = true;
			}
		});
	return View;
});