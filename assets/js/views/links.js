define([
	'jquery',
	'underscore',
	'backbone',
], function($, _, Backbone){
	var linkTpl = _.template('<li> <a href="<%= src %>"> <%= title %> </a> </li>');

	var View = Backbone.View.extend({
			initialize: function(data){
				this.$el = $('#links');
				this.render(data);
			},
			render: function(links){
				var markup = _.map(links, function(link){
						return linkTpl(link);
					});
				this.$el.html(markup);
			}
		});
	return View;
});