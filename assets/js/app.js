define([
	'jquery',
	'underscore',
	'backbone',
	'collections/slides',
	'models/audio',
	'views/links',
	// 'json!/data/data.js'
], function($, _, Backbone, Slides, Audio, LinksView){
	var proto = {
			init: function (data) {
				this.meta = data.meta;

				this.audio = new Audio(data.audio);
				this.slides = new Slides(data.slides);
				
				this.links = new LinksView(data.links);

				return this;
			},
			play: function(){
				this.audio.play();
			},
			pause: function(){
				this.audio.pause();
			},
			reset: function(){
				this.audio.stop();
			}
		};
	
	var App = (function () {
			function F() {};

			return function ( ) {
				F.prototype = proto;
				return new F();
			};
		})();

	return App;
});
